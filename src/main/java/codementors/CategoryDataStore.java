package codementors;

import codementors.model.Category;
import codementors.model.Item;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CategoryDataStore {
    private static final Logger log = Logger.getLogger(ItemDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;
    public List<Category> getCategories() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        query.from(Category.class);
        return em.createQuery(query).getResultList();
    }

    public void deleteCategory(Category category) {
        em.remove(em.merge(category));
    }

}
