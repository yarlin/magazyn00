package codementors.view.AddingElements;



import codementors.ItemDataStore;
import codementors.model.Category;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class AddCategory implements Serializable {

    @EJB
    private ItemDataStore store;
    private Category category = new Category();

    public void saveCategory() {
        store.createCategory(category);
    }

    public ItemDataStore getStore() {
        return store;
    }

    public void setStore(ItemDataStore store) {
        this.store = store;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}