package codementors.view.AddingElements;

import codementors.ItemDataStore;
import codementors.model.Item;
import sun.misc.IOUtils;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@ViewScoped ///request scope
public class AddItem implements Serializable {
    private static final Logger log = Logger.getLogger(AddItem.class.getName());

    @EJB
    private ItemDataStore store;

    private Item item = new Item();

    private List<SelectItem> warehouse;
    public List<SelectItem> getWarehouse() {
        if (warehouse == null) {
            warehouse = new ArrayList<SelectItem>();
            store.getWarehouse().forEach(iter -> {
                warehouse.add(new SelectItem(iter, iter.getName()));
            });
        }
        return warehouse;
    }



    private List<SelectItem> department;

    public List<SelectItem> getDepartment() {
        if (department == null) {
            department = new ArrayList<SelectItem>();
            store.getDepartment().forEach(iter -> {
                department.add(new SelectItem(iter, iter.getName()));
            });
        }
        return department;
    }



    private List<SelectItem> category;

    public List<SelectItem> getCategory() {
        if (category == null) {
            category = new ArrayList<SelectItem>();
            store.getCategory().forEach(iter -> {
                category.add(new SelectItem(iter, iter.getName()));
            });
        }
        return category;
    }

    private List<SelectItem> tag;

    public List<SelectItem> getTag() {
        if (tag == null) {
            tag = new ArrayList<SelectItem>();
            store.getTag().forEach(iter -> {
                tag.add(new SelectItem(iter, iter.getName()));
            });
        }
        return tag;
    }

    public void saveItem() {
        store.createItem(item);
    }

    public ItemDataStore getStore() {
        return store;
    }

    public void setStore(ItemDataStore store) {
        this.store = store;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setWarehouse(List<SelectItem> warehouse) {
        this.warehouse = warehouse;
    }


}
