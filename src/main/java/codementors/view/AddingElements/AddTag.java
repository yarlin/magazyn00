package codementors.view.AddingElements;

import codementors.ItemDataStore;
import codementors.model.Tag;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class AddTag implements Serializable {

    @EJB
    private ItemDataStore store;

    private Tag tag = new Tag();

    public void saveTag() {
        store.createTag(tag);
    }

    public ItemDataStore getStore() {
        return store;
    }

    public void setStore(ItemDataStore store) {
        this.store = store;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

}
