package codementors.view.AddingElements;


import codementors.UserDataStore;
import codementors.model.User;

import javax.ejb.EJB;

import javax.faces.model.SelectItem;

import javax.faces.view.ViewScoped;

import javax.inject.Named;

import java.io.Serializable;

import java.util.ArrayList;

import java.util.List;



@Named

@ViewScoped

public class AddUser implements Serializable {



    @EJB

    private UserDataStore store;



    private User user = new User();



    private List<SelectItem> roles;





    public void addNewUser() {

        store.createUser(user);

    }

    public User getUser() {

        return user;

    }



    public void setUser(User user) {

        this.user = user;

    }



    public List<SelectItem> getRoles() {

        if (roles == null) {

            roles = new ArrayList<SelectItem>();

            for (User.Role role : User.Role.values()) {

                roles.add(new SelectItem(role, role.name()));

            }



        }

        return roles;

    }

}

