package codementors.view.Context;

import codementors.model.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;

@Singleton
@Startup
public class UserDataStoreContext {

    private static final Logger log = Logger.getLogger(UserDataStoreContext.class.getName());

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {

        User user1 = new User("user", DigestUtils.sha256Hex("user"), "adam", "zuk", User.Role.USER);
        User user2 = new User("kiera", DigestUtils.sha256Hex("dupa"), "ada", "zzak", User.Role.USER);
        User user3 = new User("era", DigestUtils.sha256Hex("dupa"), "adaka", "szak", User.Role.USER);
        User user4 = new User("admin", DigestUtils.sha256Hex("admin"), "adaka", "szak", User.Role.ADMIN);
        em.persist(user1);
        em.persist(user2);
        em.persist(user3);
        em.persist(user3);
        em.persist(user4);



    }


}