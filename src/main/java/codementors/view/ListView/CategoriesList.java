package codementors.view.ListView;


import codementors.CategoryDataStore;
import codementors.model.Category;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class CategoriesList implements Serializable {

    @EJB
    private CategoryDataStore store;

    private String newCategory;

    private List<Category> categories;

    public List<Category> getCategories() {
        if (categories == null) {
            categories=store.getCategories();
        }
        return categories;
    }

    public void delete(Category category) {
        store.deleteCategory(category);
        categories = null;
    }

}
