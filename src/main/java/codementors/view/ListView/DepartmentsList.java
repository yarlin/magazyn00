package codementors.view.ListView;


import codementors.DepartmentDataStore;
import codementors.model.Department;


import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class DepartmentsList implements Serializable {

    @EJB
    private DepartmentDataStore store;


    private List<Department> departments;

    public List<Department> getDepartments() {
        if (departments == null) {
            departments =store.getDepartments();
        }
        return departments;
    }


    public void delete(Department department) {
        store.deleteDepartment(department);
        departments = null;
    }

}
