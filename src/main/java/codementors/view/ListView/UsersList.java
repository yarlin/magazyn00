package codementors.view.ListView;


import codementors.UserDataStore;
import codementors.model.User;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class UsersList implements Serializable {

    @EJB
    private UserDataStore store;

   
    private List<User> users;

    public List<User> getUsers() {
        if (users == null) {
            users = store.getUsers();
        }
        return users;
    }


    public void delete(User user) {
        store.deleteUser(user);
        users = null;
    }

}
