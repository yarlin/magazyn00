package codementors.view.converter;

import codementors.ItemDataStore;
import codementors.model.Category;
import codementors.model.Department;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("departmentConveter")
public class DepartmentConveter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        ItemDataStore store = CDI.current().select(ItemDataStore.class).get();
        return store.getDepartment(Integer.parseInt(value));//Parse id in string to integer.

    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return ((Department) o).getId() + "";
    }
}